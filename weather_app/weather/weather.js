const request = require("request");

var getWeather = function(lat, lng, callback){
    request({url:'https://api.darksky.net/forecast/e0fc6e50033705356a3c7e028c1759da/'+ lat + ',' + lng, json:true}, 
    function(error, response, body){
        if(!error && response.statusCode == 200){
            //console.log('Temperature ' + body.currently.temperature);
            callback(undefined, {
                temperature: body.currently.temperature,
                apparentTemperature: body.currently.apparentTemperature
            });
        }
        else{
            //console.log('An error has occurred ' + error);
            callback('An error has occurred ' + error);
        }
    });
};

module.exports.getWeather = getWeather;
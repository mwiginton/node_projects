const request = require("request");
const yargs = require('yargs');


var geocodeAddress = function (address, callback){
    // encode user input to pass to url string
    var encodedAddress = encodeURIComponent(address);

    request({url:'http://www.mapquestapi.com/geocoding/v1/address?key=4HfPfIbYfYAEjYSo0tjwCBGcSFHkZTVw&location='+encodedAddress, json:true}, 
    function(error, response, body){
        if(error){
            callback('An error has occured ' + error);
        }
        if(body.status == "ZERO_RESULTS"){
            callback("no results returned");
        }
        if(body.results.length > 0)
        {
            callback(undefined,{
                address: body.results[0].locations[0].street + ' ' + body.results[0].locations[0].adminArea5 + ' ' + body.results[0].locations[0].adminArea3 + ' ' + body.results[0].locations[0].postalCode,
                latitude: body.results[0].locations[0].latLng.lat,
                longitude: body.results[0].locations[0].latLng.lng
            });
        }
    });
}

module.exports.geocodeAddress = geocodeAddress;
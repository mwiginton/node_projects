const yargs = require('yargs');
const geocode = require('./geocode/geocode.js');
const weather = require('./weather/weather.js');

const argv = yargs.options({
    a:{demand:true, alias: 'address', describe:'Address to fetch weather for', string:true}
}).help().alias('help', 'h').argv;

console.log("Weather Application");
geocode.geocodeAddress(argv.a, function(error, results){
    if(error){
        console.log('an error has occured: ' + error);
    } else {
        console.log('Address: ' + results.address);
        weather.getWeather(results.latitude, results.longitude, function(error, result){
            if(error){
                console.log('an error has occured ' + error);
            }
            else {
                console.log(result);
            }
        });
    }
});

// https://api.darksky.net/forecast/[key]/[latitude],[longitude]
// https://api.darksky.net/forecast/e0fc6e50033705356a3c7e028c1759da/29.757,-95.391015
// api key: e0fc6e50033705356a3c7e028c1759da
// lat: 29.757 lng: -95.391015
// weather.getWeather(29.757, -95.391015, function(error, result){
//     if(error){
//         console.log('an error has occured ' + error);
//     }
//     else {
//         console.log(result);
//     }
// });
